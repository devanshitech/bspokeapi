<?php
class FeedsController extends Controller
{
    
    public function addFeedAction($request) {
        
        //$data='{"user_id":"1","title":"Test","description":"Test","image":"test.png","audio":"test.wave","tags":"Business,Technology,Culture"}';	    
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));              
        
        if ($data->user_id == "" || strlen($data->user_id) == 0 ) 
            throw new Exception('Please enter id', Response::BAD_REQUEST);

        if ($data->title == "" || strlen($data->title) == 0 ) 
            throw new Exception('Please enter title', Response::BAD_REQUEST);

        if ($data->description == "" || strlen($data->description) == 0 ) 
            throw new Exception('Please enter description', Response::BAD_REQUEST);

        $feed = array();
        $feed['user_id'] = $data->user_id;
        $feed['title'] = $data->title;
        $feed['description'] = $data->description;
        $feed['image'] = $data->image;
        $feed['audio'] = $data->audio;
        $feed['tags'] = $data->tags;
        $feed['created_at'] = date("Y-m-d H:i:s");
        
        $model = $this->getModel('Feeds');
        
        try {
            $feedObj = new Feeds($feed);
            $id = $model->save($feedObj);            
        } catch (ValidationException $e) {
            throw new Exception($e->getMessage(), Response::UNAUTHORIZED);
        }

        $response = new Response();
        $response->message = "Feed Created Successfully.";
        $response->feed_id = $id;
        $response->setCode(Response::CREATED);
        return $response;
    }

    public function getListenAction($request){
        //$data='{"feed_id":"1","user_id":"1"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));

        $model = $this->getModel('FeedListens');
        
        $chkListen = $model->findBy(array('feed_id'=>$data->feed_id,'user_id'=>$data->user_id));

        if(!$chkListen)
        {
            $feedListens = array();
            $feedListens['feed_id'] = $data->feed_id;
            $feedListens['user_id'] = $data->user_id;
            $feedListens['created_at'] = date("Y-m-d H:i:s");
            $feedListensObj = new FeedListens($feedListens);
            $model->save($feedListensObj);
        }
        
        $response = new Response();
        $response->message = "Feed listend successfully.";
        $response->setCode(Response::OK);
        return $response;
    }

    public function getLikeAction($request){
        //$data='{"feed_id":"1","user_id":"1","flag":"1"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));

        $model = $this->getModel('FeedLikes');
        $likeID = $model->findBy(array('feed_id'=>$data->feed_id,'user_id'=>$data->user_id));
        if($data->flag == 1)
        {
            if(!$likeID)
            {
                $feedLikes = array();
                $feedLikes['feed_id'] = $data->feed_id;
                $feedLikes['user_id'] = $data->user_id;
                $feedLikes['created_at'] = date("Y-m-d H:i:s");
                $feedLikesObj = new FeedLikes($feedLikes);
                $model->save($feedLikesObj);
                $message = 'Feed liked successfully.';
            }
            else{
                $message = 'Feed already liked.';   
            }
        }
        else {
            $model->delete($likeID->id);
            $message = 'Feed unliked successfully.';
        }
        
        $response = new Response();
        $response->message = $message;
        $response->feed_id = $data->feed_id;
        $response->flag = $data->flag;
        $response->setCode(Response::OK);
        return $response;
    }

    public function getFeedAction($request){
        //$request->acceptContentTypes(array('json', 'xml'));
        //$data = json_decode(file_get_contents("php://input"));

        $feedObj = new FeedsModel(Loader::getInstance()->getDatabase());
        $feedArray = $feedObj->getFeeds();

        $response = new Response();
        $response->message = "Feed list";
        $response->feeds = $feedArray;
        $response->setCode(Response::OK);
        return $response;
    }

    public function addResponseAction($request){
        //$data='{"feed_id":"1","user_id":"1","response_text":"test"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));

        if ($data->user_id == "" || strlen($data->user_id) == 0 ) 
            throw new Exception('Please enter id', Response::BAD_REQUEST);

        if ($data->response_text == "" || strlen($data->response_text) == 0 ) 
            throw new Exception('Please enter response text', Response::BAD_REQUEST);

        $model = $this->getModel("FeedResponses");
        $feedResponse = array();
        $feedResponse['feed_id'] = $data->feed_id;
        $feedResponse['user_id'] = $data->user_id;
        $feedResponse['response_text'] = $data->response_text;
        $feedResponse['created_at'] = date("Y-m-d H:i:s");
        $responseObj = new FeedResponses($feedResponse);
        $model->save($responseObj);

        $response = new Response();
        $response->message = "Response added successfully.";
        $response->setCode(Response::CREATED);
        return $response;
    }

    public function getFeedByTagAction($request){
        //$data='{"tag":"Business"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));            

        if ($data->tag == "" || strlen($data->tag) == 0 ) 
            throw new Exception('Please enter tag', Response::BAD_REQUEST);

        $feedObj = new FeedsModel(Loader::getInstance()->getDatabase());
        $feedArray = $feedObj->getFeedListByTag($data->tag);

        if (count($feedArray) == 0) {
            throw new Exception('Feed not found', Response::UNAUTHORIZED);
        }

        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Feed list";
        $response->feeds = $feedArray;
        return $response;
    }
}
