<?php
class UsersController extends Controller
{
    
    public function indexAction($request)
    {
        // serve HTML, JSON and XML
        $request->acceptContentTypes(array('html', 'json', 'xml'));
        
        if ('html' == $request->getContentType()) {
            $response = new View();
            $response->setLayout('main');
        } else {
            $response = new Response();
        }
        
        $response->users = $this->getModel('User')->findAll();
        return $response;
    }
    
    public function showAction($request)
    {
        // serve HTML, JSON and XML
        $request->acceptContentTypes(array('html', 'json', 'xml'));
        
        $model = $this->getModel('User');
        $id = $request->getParam('id');
        $user = is_numeric($id) ? $model->find($id) : $model->findBy(array('username'=>$id));
        if (! $user) {
            throw new Exception('User not found', Response::NOT_FOUND);
        }
        
        if ('html' == $request->getContentType()) {
            $response = new View();
            $response->setLayout('main');
        } else {
            $response = new Response();
            $response->setEtagHeader(md5('/users/' . $user->id));
        }
        
        $response->user = $user; 
        return $response;
    }
    
    public function signupAction($request) {      
        //$data='{"name":"Niraj Radadiya","email":"niraj@techflitter.com","password":"admin123"}';	    
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));              
        //$data = json_decode($data);

        if ($data->name == "" || strlen($data->name) == 0 ) 
            throw new Exception('Please enter name', Response::BAD_REQUEST);

        if ($data->email == "" || strlen($data->email) == 0 ) 
            throw new Exception('Please enter email', Response::BAD_REQUEST);

        if ($data->password == "" || strlen($data->password) == 0 ) 
            throw new Exception('Please enter password', Response::BAD_REQUEST);

        $model = $this->getModel('Users');

        $chkUser = $model->findBy(array('email' => $data->email));
        if ($chkUser) {
            throw new Exception('User already exist', Response::NOT_ALLOWED);
        }

        //insert into user
        $user = array();
        if($_FILES['image']['tmp_name'] != "") {   
            $imageName = $_FILES['image']['name'];
            $tmp_name = $_FILES["image"]["tmp_name"];            
            $path='images/'.$imageName;
            move_uploaded_file($tmp_name,$path);
            $user['image'] = $imageName;
        }
        $user['name'] = $data->name;
        $user['email'] = $data->email;
        $user['password'] = $data->password;
        $user['created_at'] = date("Y-m-d H:i:s");
        $user['updated_at'] = date("Y-m-d H:i:s");
        $user['login_at'] = date("Y-m-d H:i:s");
        
        try {
            $userObj = new Users($user);
            $id = $this->getModel('Users')->save($userObj);            
        } catch (ValidationException $e) {
            throw new Exception($e->getMessage(), Response::UNAUTHORIZED);
        }

        $response = new Response();
        $response->message = "User Created Successfully.";
        $response->user_id = $id;
        $response->setCode(Response::CREATED);
        return $response;
    }
    
    public function loginFacebookAction($request) {
        
        //$data='{"name":"Sanjay Prajapati","fbid":"17276844853333dfdf111212","email":"sanjay22@gmail.com","image":"facebookprofile.png"}';	            
        $request->acceptContentTypes(array('json', 'xml', 'html'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->fbid == "") 
            throw new Exception('Please enter fbuser information', Response::BAD_REQUEST);

        $model = $this->getModel('Users');
        $chkUser = $model->findBy(array('email' => $data->email));
        if ($chkUser) {
            throw new Exception('User already exist', Response::NOT_ALLOWED);
        }
           
        //insert into user
        $user = array();
        $user['name'] = $data->name;
        $user['fbid'] = $data->fbid;
        $user['email'] = $data->email;
        $user['image'] = $data->image;
        $user['created_at'] = date("Y-m-d H:i:s");
        $user['updated_at'] = date("Y-m-d H:i:s");
        $user['login_at'] = date("Y-m-d H:i:s");
        
        $chkFbUser = $model->findBy(array('fbid' => $data->fbid));
        
        if ($chkFbUser) {
            
            //update user table login field
            $chkFbUser->login_at =  date("Y-m-d H:i:s");
            $model->save($chkFbUser);
            
            $response = new Response();
            $response->setCode(Response::CREATED);
            $response->user_id = $chkFbUser->id;
            $response->email = $chkFbUser->email;
            return $response;
        }

        try {
            $userObj = new Users($user);
            $id = $this->getModel('Users')->save($userObj);
        } catch (ValidationException $e) {
            throw new Exception($e->getMessage(), Response::CREATED);
        }
        
        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Login Successfully.";
        $response->user_id = $id;
        return $response;
    }

    public function loginTwitterAction($request) {
        
        //$data='{"name":"Sanjay Prajapati","twid":"17276844853333dfdf111212","email":"sanjay22@gmail.com","image":"twitterprofile.png"}';                
        $request->acceptContentTypes(array('json', 'xml', 'html'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->twid == "") 
            throw new Exception('Please enter twitter user information', Response::BAD_REQUEST);
           
        //insert into user
        $user = array();
        $user['name'] = $data->name;
        $user['twid'] = $data->twid;
        $user['email'] = $data->email;
        $user['image'] = $data->image;
        $user['created_at'] = date("Y-m-d H:i:s");
        $user['updated_at'] = date("Y-m-d H:i:s");
        $user['login_at'] = date("Y-m-d H:i:s");
        
        $model1 = $this->getModel('Users');
        $chkFbUser = $model1->findBy(array('twid' => $data->twid));
        
        if ($chkFbUser) {
            
            //update user table login field
            $chkFbUser->login_at =  date("Y-m-d H:i:s");
            $model1->save($chkFbUser);
            
            $response = new Response();
            $response->setCode(Response::CREATED);
            $response->user_id = $chkFbUser->id;
            $response->email = $chkFbUser->email;
            return $response;
        }

        try {
            $userObj = new Users($user);
            $id = $this->getModel('Users')->save($userObj);
        } catch (ValidationException $e) {
            throw new Exception($e->getMessage(), Response::CREATED);
        }
        
        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Login Successfully.";
        $response->user_id = $id;
        return $response;
    }

    public function loginGoogleAction($request) {
        
        //$data='{"name":"Sanjay Prajapati","gid":"17276844853333dfdf111212","email":"sanjay22@gmail.com","image":"googleprofile.png"}';                
        $request->acceptContentTypes(array('json', 'xml', 'html'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->gid == "") 
            throw new Exception('Please enter google user information', Response::BAD_REQUEST);
           
        //insert into user
        $user = array();
        $user['name'] = $data->name;
        $user['gid'] = $data->gid;
        $user['email'] = $data->email;
        $user['image'] = $data->image;
        $user['created_at'] = date("Y-m-d H:i:s");
        $user['updated_at'] = date("Y-m-d H:i:s");
        $user['login_at'] = date("Y-m-d H:i:s");
        
        $model1 = $this->getModel('Users');
        $chkFbUser = $model1->findBy(array('gid' => $data->gid));
        
        if ($chkFbUser) {
            
            //update user table login field
            $chkFbUser->login_at =  date("Y-m-d H:i:s");
            $model1->save($chkFbUser);
            
            $response = new Response();
            $response->setCode(Response::CREATED);
            $response->user_id = $chkFbUser->id;
            $response->email = $chkFbUser->email;
            return $response;
        }

        try {
            $userObj = new Users($user);
            $id = $this->getModel('Users')->save($userObj);
        } catch (ValidationException $e) {
            throw new Exception($e->getMessage(), Response::CREATED);
        }
        
        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Login Successfully.";
        $response->user_id = $id;
        return $response;
    }
    
    public function loginAction($request) {
        
        //$data='{"email":"niraj@techflitter.com","password":"admin123"}';	   
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->email == "" || strlen($data->email) == 0) 
            throw new Exception('Please enter username', Response::BAD_REQUEST);        
        if ($data->password == "" || strlen($data->password) == 0) 
            throw new Exception('Please enter password', Response::BAD_REQUEST);                
        
        $user = array();
        $user['email'] = $data->email;
        $user['password'] = $data->password;
                
        $model1 = $this->getModel('Users');
        $chkUser = $model1->findBy(array('email' => $data->email));        
       
        if (!$chkUser) {
            throw new Exception('User not found', Response::BAD_REQUEST);
        }
        
        if($chkUser->password != $data->password) {
            throw new Exception('Password not matched', Response::BAD_REQUEST);
        }
        
        $chkUser->login_at =  date("Y-m-d H:i:s");
        $model1->save($chkUser);
            
        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Login Successfully.";
        $response->user_id = $chkUser->id;
        return $response;
    }

    public function updateProfileAction($request){
        //$data='{"user_id":"1","name":"Niraj Radadiya","email":"niraj@techflitter.com","password":"admin123"}';
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->name == "" || strlen($data->name) == 0 ) 
            throw new Exception('Please enter name', Response::BAD_REQUEST);

        if ($data->email == "" || strlen($data->email) == 0 ) 
            throw new Exception('Please enter email', Response::BAD_REQUEST);

        if ($data->password == "" || strlen($data->password) == 0 ) 
            throw new Exception('Please enter password', Response::BAD_REQUEST);

        $model = $this->getModel('Users');
        $chkUser = $model->findBy(array('id' => $data->user_id));
        
        if($chkUser) {
            if($_FILES['image']['tmp_name'] != "") 
            {   
                $imageName = $_FILES['image']['name'];
                unlink('images/'.$imageName);
                $tmp_name = $_FILES["image"]["tmp_name"];            
                $path='images/'.$imageName;
                move_uploaded_file($tmp_name,$path);
                $chkUser->image = $imageName;
            }

            $chkUser->name =  $data->name;
            $chkUser->email =  $data->email;
            $chkUser->password =  $data->password;
            $chkUser->updated_at = date("Y-m-d H:i:s");
            $model->save($chkUser);
        }

        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Your profile updated Successfully";
        return $response;
    }
    
    public function changePasswordAction($request){

        //$data='{"user_id":"1","current_password":"admin123","new_password":"test123","confirm_password":"test123"}';
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));
        //$data = json_decode($data);

        if ($data->user_id == "" || strlen($data->user_id) == 0)
            throw new Exception('Please enter id', Response::BAD_REQUEST);
        
        if ($data->current_password == "" || strlen($data->current_password) == 0)
            throw new Exception('Please enter current password', Response::BAD_REQUEST);
        
        if ($data->new_password == "" || strlen($data->new_password) == 0)
            throw new Exception('Please enter new password', Response::BAD_REQUEST);
        
        if ($data->confirm_password == "" || strlen($data->confirm_password) == 0)
            throw new Exception('Please enter confirm password', Response::BAD_REQUEST);
        
        if ($data->new_password != $data->confirm_password) {
            throw new Exception('Password not match.', Response::NOT_ALLOWED);
        }

        $model = $this->getModel('Users');
        
        $chkPass = $model->findBy(array('id' => $data->user_id,'password' => $data->current_password));
        if (!$chkPass) {
            throw new Exception('Current password wrong.', Response::NOT_ALLOWED);
        }

        $chkUser = $model->findBy(array('id' => $data->user_id));
        
        if($chkUser) {
            $chkUser->password =  $data->new_password;
            $chkUser->updated_at = date("Y-m-d H:i:s");
            $model->save($chkUser);
        }
        
        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Your password has been updated";
        return $response;           
    }

    public function getFriendAction($request){
        //$data='{"user_id":"1"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));

        if ($data->user_id == "" || strlen($data->user_id) == 0 ) 
            throw new Exception('Please enter id', Response::BAD_REQUEST);

        $objFriend = new FollowFriendsModel(Loader::getInstance()->getDatabase());
        $friendsArray = $objFriend->getFriendList($data->user_id);

        $response = new Response();
        $response->setCode(Response::OK);
        $response->friendlist=$friendsArray;
        return $response;
    }

    public function followFriendAction($request){
        //$data='{"user_id":"1","friend_id":"1,2,3"}';       
        $request->acceptContentTypes(array('json', 'xml'));
        $data = json_decode(file_get_contents("php://input"));

        if ($data->user_id == "" || strlen($data->user_id) == 0 ) 
            throw new Exception('Please enter id', Response::BAD_REQUEST);

        if ($data->friend_id == "" || strlen($data->friend_id) == 0 ) 
            throw new Exception('Please enter friends', Response::BAD_REQUEST);

        $model = $this->getModel("FollowFriends");

        $chkFollow = $model->findBy(array('user_id'=>$data->user_id));
        if($chkFollow)
        {
            $chkFollow->friend_id =  $data->friend_id;
            $chkFollow->updated_at = date("Y-m-d H:i:s");
            $model->save($chkFollow);
        } else {
            $followFriends = array();
            $followFriends['user_id'] = $data->user_id;
            $followFriends['friend_id'] = $data->friend_id;
            $followFriends['created_at'] = date("Y-m-d H:i:s");
            $followFriendsObj = new FollowFriends($followFriends);
            $model->save($followFriendsObj);
        }

        $response = new Response();
        $response->setCode(Response::OK);
        $response->message = "Friends follow successfully.";
        $response->following = $data->friend_id;
        return $response;
    }
}
