<?php
class FeedsModel extends Model
{
	public function getFeeds() {
        $selectFeeds = $this->db->prepare("SELECT f.*,
        	(select count(fl.id) from feed_listens fl where fl.feed_id=f.id) as totallistens,
        	(select count(flk.id) from feed_likes flk where flk.feed_id=f.id) as totallikes,
        	(select count(fr.id) from feed_responses fr where fr.feed_id=f.id) as totalresponse 
        	FROM feeds f");
        $selectFeeds->execute();
        $result = $selectFeeds->fetchAll(PDO::FETCH_CLASS);
        return $result;
    }

    public function getFeedListByTag($tag){
        $selectFeeds = $this->db->prepare("SELECT f.*,
            (select count(fl.id) from feed_listens fl where fl.feed_id=f.id) as totallistens,
            (select count(flk.id) from feed_likes flk where flk.feed_id=f.id) as totallikes,
            (select count(fr.id) from feed_responses fr where fr.feed_id=f.id) as totalresponse 
            FROM feeds f
            WHERE FIND_IN_SET('".$tag."',f.tags)");
        $selectFeeds->execute();
        $result = $selectFeeds->fetchAll(PDO::FETCH_CLASS);
        return $result;    
    }
}