<?php

$redirect = explode('/',$_SERVER['REQUEST_URI']);

$routes[] = new Route('/example/:action', 
    array(
        'controller' => 'index'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/signup', 
    array(
        'controller' => 'users',
        'action'     => 'signup'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/loginFacebook', 
    array(
        'controller' => 'users',
        'action'     => 'loginFacebook'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/loginTwitter', 
    array(
        'controller' => 'users',
        'action'     => 'loginTwitter'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/loginGoogle', 
    array(
        'controller' => 'users',
        'action'     => 'loginGoogle'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/login', 
    array(
        'controller' => 'users',
        'action'     => 'login'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/updateProfile', 
    array(
        'controller' => 'users',
        'action'     => 'updateProfile'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/changePassword', 
    array(
        'controller' => 'users',
        'action'     => 'changePassword'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/addFeed', 
    array(
        'controller' => 'feeds',
        'action'     => 'addFeed'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/getListen', 
    array(
        'controller' => 'feeds',
        'action'     => 'getListen'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/getLike', 
    array(
        'controller' => 'feeds',
        'action'     => 'getLike'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/getFeed', 
    array(
        'controller' => 'feeds',
        'action'     => 'getFeed'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/addResponse', 
    array(
        'controller' => 'feeds',
        'action'     => 'addResponse'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/followFriend', 
    array(
        'controller' => 'users',
        'action'     => 'followFriend'
    )
);

$routes[] = new Route('/'.$redirect[1].'/users/getFriend', 
    array(
        'controller' => 'users',
        'action'     => 'getFriend'
    )
);

$routes[] = new Route('/'.$redirect[1].'/feeds/getFeedByTag', 
    array(
        'controller' => 'feeds',
        'action'     => 'getFeedByTag'
    )
);
return $routes;
